/*
    SPDX-FileCopyrightText: 2023 Aaron Rainbolt <arraybolt3@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "vncviewitem.h"

#include <QApplication>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{
    qmlRegisterType<VncViewItem>("vncviewitem", 1, 0, "VncViewItem");
    QApplication app(argc, argv);
    QQmlApplicationEngine engine(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    return app.exec();
}
