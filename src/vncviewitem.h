/*
    SPDX-FileCopyrightText: 2023 Aaron Rainbolt <arraybolt3@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef VNCVIEWITEM_H
#define VNCVIEWITEM_H

#include "vncclientthread.h"

#include <QQuickPaintedItem>

class QPainter;
class QMouseEvent;
class QHoverEvent;
class QKeyEvent;

class VncViewItem : public QQuickPaintedItem
{
    Q_OBJECT

public:
    explicit VncViewItem(QQuickItem *parent = nullptr);
    virtual ~VncViewItem();

    void paint(QPainter *painter) override;
    Q_INVOKABLE void vncConnect(QUrl vncUrl);

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent* event) override;
    void hoverMoveEvent(QHoverEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;

private:
    VncClientThread *m_vncthread;
    QMap<unsigned int, bool> m_mods;
    int m_buttonMask;
    int m_wheelRemainderV;
    int m_wheelRemainderH;
    void doMouse(QMouseEvent* e);
    void doMouse(QHoverEvent *event);
    void doKey(QKeyEvent *e);
    void unpressModifiers();

private Q_SLOTS:
    void updateImage(int x, int y, int w, int h);
};

#endif
