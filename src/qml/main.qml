/*
    SPDX-FileCopyrightText: 2023 Aaron Rainbolt <arraybolt3@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0 as QQC2
import org.kde.kirigami 2.15 as Kirigami
import vncviewitem 1.0

Kirigami.ApplicationWindow {
    pageStack.initialPage: [ vncPage ]

    Kirigami.Page {
        id: vncPage

        ColumnLayout {
            id: vncLayout
            anchors.fill: parent

            Item {
                Layout.alignment: Qt.AlignHCenter
                implicitWidth: vncDisplay.implicitWidth
                implicitHeight: vncDisplay.implicitHeight

                Rectangle {
                    id: vncBackground
                    implicitWidth: vncDisplay.implicitWidth
                    implicitHeight: vncDisplay.implicitHeight
                    color: "black"
                }

                VncViewItem {
                    id: vncDisplay
                    Component.onCompleted: this.vncConnect("vnc://127.0.0.1:5900")
                    width: 1920
                    height: 1080
                }
            }
        }
    }
}
