/*
    SPDX-FileCopyrightText: 2023 Aaron Rainbolt <arraybolt3@gmail.com>
    SPDX-FileCopyrightText: 2007-2013 Urs Wolfer <uwolfer@kde.org>
    SPDX-FileCopyrightText: 2021 Rafał Lalik <rafallalik@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "vncviewitem.h"
#include "remoteview.h"

#include <QPainter>
#include <QImage>
#include <QMap>
#include <QCursor>

VncViewItem::VncViewItem(QQuickItem *parent) : QQuickPaintedItem(parent)
{
    setFlag(QQuickItem::ItemHasContents);
    setAcceptedMouseButtons(Qt::AllButtons);
    setAcceptHoverEvents(true);
    m_buttonMask = 0;
}

VncViewItem::~VncViewItem()
{
}

void VncViewItem::paint(QPainter *painter)
{
    QImage vncFrame;
    vncFrame = m_vncthread->image();
    setImplicitWidth(vncFrame.width());
    setImplicitHeight(vncFrame.height());
    painter->drawImage(QPoint(), vncFrame);
}

void VncViewItem::vncConnect(QUrl vncUrl)
{
    m_vncthread = new VncClientThread(nullptr);
    m_vncthread->setHost(vncUrl.host());
    m_vncthread->setPort(vncUrl.port());
    m_vncthread->setQuality(RemoteView::High);
    m_vncthread->start();
    m_vncthread->setShowLocalCursor(true);
    setCursor(Qt::ArrowCursor);
    connect(m_vncthread, SIGNAL(imageUpdated(int,int,int,int)), this, SLOT(updateImage(int,int,int,int)), Qt::BlockingQueuedConnection);
    connect(m_vncthread, &VncClientThread::gotCursor, this, [this](QCursor cursor){ setCursor(cursor); });
}

void VncViewItem::updateImage(int x, int y, int w, int h)
{
    update();
}

void VncViewItem::mousePressEvent(QMouseEvent *event)
{
    doMouse(event);
}

void VncViewItem::mouseReleaseEvent(QMouseEvent *event)
{
    doMouse(event);
}

void VncViewItem::mouseMoveEvent(QMouseEvent *event)
{
    doMouse(event);
}

void VncViewItem::mouseDoubleClickEvent(QMouseEvent *event)
{
    doMouse(event);
}

void VncViewItem::hoverMoveEvent(QHoverEvent *event)
{
    doMouse(event);
}

void VncViewItem::doMouse(QMouseEvent *e) // from KRDC
{
    forceActiveFocus();
        if (e->type() != QEvent::MouseMove) {
        if ((e->type() == QEvent::MouseButtonPress) ||
                (e->type() == QEvent::MouseButtonDblClick)) {
            if (e->button() & Qt::LeftButton)
                m_buttonMask |= 0x01;
            if (e->button() & Qt::MiddleButton)
                m_buttonMask |= 0x02;
            if (e->button() & Qt::RightButton)
                m_buttonMask |= 0x04;
            if (e->button() & Qt::ExtraButton1)
                m_buttonMask |= 0x80;
        } else if (e->type() == QEvent::MouseButtonRelease) {
            if (e->button() & Qt::LeftButton)
                m_buttonMask &= 0xfe;
            if (e->button() & Qt::MiddleButton)
                m_buttonMask &= 0xfd;
            if (e->button() & Qt::RightButton)
                m_buttonMask &= 0xfb;
            if (e->button() & Qt::ExtraButton1)
                m_buttonMask &= ~0x80;
        }
    }

    QPointF loc = e->localPos();
    m_vncthread->mouseEvent(loc.x(), loc.y(), m_buttonMask);
}

void VncViewItem::doMouse(QHoverEvent *event)
{
    forceActiveFocus();
    m_vncthread->mouseEvent(event->pos().x(), event->pos().y(), 0);
}

void VncViewItem::keyPressEvent(QKeyEvent *event)
{
    doKey(event);
}

void VncViewItem::keyReleaseEvent(QKeyEvent *event)
{
    doKey(event);
}

void VncViewItem::doKey(QKeyEvent *e) // From KRDC
{
    // strip away autorepeating KeyRelease; see bug #206598
    if (e->isAutoRepeat() && (e->type() == QEvent::KeyRelease))
        return;

// parts of this code are based on https://github.com/veyon/veyon/blob/master/core/src/VncView.cpp
    rfbKeySym k = e->nativeVirtualKey();

    // we do not handle Key_Backtab separately as the Shift-modifier
    // is already enabled
    if (e->key() == Qt::Key_Backtab) {
        k = XK_Tab;
    }

    const bool pressed = (e->type() == QEvent::KeyPress);

    // handle modifiers
    if (k == XK_Shift_L || k == XK_Control_L || k == XK_Meta_L || k == XK_Alt_L || XK_Super_L || XK_Hyper_L ||
        k == XK_Shift_R || k == XK_Control_R || k == XK_Meta_R || k == XK_Alt_R || XK_Super_R || XK_Hyper_R) {
        if (pressed) {
            m_mods[k] = true;
        } else if (m_mods.contains(k)) {
            m_mods.remove(k);
        } else {
            unpressModifiers();
        }
    }

    if (k) {
        m_vncthread->keyEvent(k, pressed);
    }
}

void VncViewItem::unpressModifiers() // From KRDC
{
    const QList<unsigned int> keys = m_mods.keys();
    QList<unsigned int>::const_iterator it = keys.constBegin();
    while (it != keys.end()) {
        m_vncthread->keyEvent(*it, false);
        it++;
    }
    m_mods.clear();
}

void VncViewItem::wheelEvent(QWheelEvent *event) // From KRDC
{
    const auto delta = event->angleDelta();
    // Reset accumulation if direction changed
    const int accV = (delta.y() < 0) == (m_wheelRemainderV < 0) ? m_wheelRemainderV : 0;
    const int accH = (delta.x() < 0) == (m_wheelRemainderH < 0) ? m_wheelRemainderH : 0;
    // A wheel tick is 15° or 120 eights of a degree
    const int verTicks = (delta.y() + accV) / 120;
    const int horTicks = (delta.x() + accH) / 120;
    m_wheelRemainderV = (delta.y() + accV) % 120;
    m_wheelRemainderH = (delta.x() + accH) % 120;

    const QPointF pos = event->position();

    const int x = qRound(pos.x());
    const int y = qRound(pos.y());

    int eb = verTicks < 0 ? 0x10 : 0x08;
    m_vncthread->mouseEvent(x, y, eb | m_buttonMask);
    m_vncthread->mouseEvent(x, y, m_buttonMask);
}
