# qml-vnc - A dirt-simple VNC client using Qt Quick and Kirigami

This is the result of an experiment to see whether KRDC's VNC code could be used within Qt Quick and Kirigami. Turns out the answer is "yes", but it took quite a bit of hacking to make things work right.

qml-vnc is basically just a Qt Quick compatible VNC widget wrapped in a Kirigami window. The application automatically connects to vnc://127.0.0.1:5900 on startup and displays the console of whatever server it connects to centered in the window. Mouse buttons, mouse movements, scrolling, and typing are all supported.

The local cursor is allowed to remain visible so that you can see what you're doing when connecting to VMs that use a "hardware" cursor (which can happen easily when running Linux VMs with QXL or VirtIO graphics). The remote cursor is also left visible, allowing you to see what it's doing if it exists. Whether this is a bug or a feature is left as an exercise to the reader.

qml-vnc relies heavily on code from KRDC (the vncclientthread.{cpp,h} and remoteview.{cpp,h} files and a bunch of code from vncview.cpp and some from vncview.h). Inspiration for the design of VncViewItem was taken from https://www.qcustomplot.com/index.php/support/forum/172, though I ended up making it into a VNC widget of its own rather than simply embedding VncView.

All the code in here is licensed under the GNU GPL 2.0, or at your option any later version, with the exception of the CMake files which are licensed under BSD-2-Clause. See the individual source code files for copyright info.

# Building

You'll need Qt and QtQuick development headers, as well as libvncserver development headers. Both Qt5 and Qt6 are supported.

    cd qml-vnc
    mkdir build && cd build
    cmake ..    # pass -DBUILD_WITH_QT6=ON for a Qt6 build, otherwise it will default to Qt5
    make -j$(nproc)

# Usage

First, launch something that listens for VNC connections on 127.0.0.1:5900. The easiest way to do this is to launch a QEMU virtual machine, using a command along the lines of:

    qemu-system-x86_64 -enable-kvm -m 4G -smp 2 -vga virtio -display egl-headless -vnc :0 -cdrom <bootable ISO file here> -usb -device usb-tablet

Replace `<bootable ISO file here>` with a path to a bootable ISO file - pretty much anything will work here but a live Linux distro is recommended.

Once you have that running, go to wherever you built qml-vnc a bit ago and run `./qml-vnc`. The VNC console should be displayed and you should be able to interact with it.

# Rough edges

Resolutions above 1920x1080 are currently unsupported - if you set a higher resolution only the upper-left corner of the display will be shown. This is intended to be fixed.

There's some forced space around the edges that Kirigami puts there by default. Not sure if this can be fixed, but it's something to be looked into.

FIXED: The mouse cursor doesn't morph into the guest's cursor, and it always remains a standard pointer.

The last frame sent by the guest will just "hang" if you shut down the guest - the VNC disconnection isn't detected properly. This is intended to be fixed.

PARTIALLY FIXED: The mouse pointer from the guest is allowed to display at the same time as the one from the host.
